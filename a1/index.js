/*console.log(`Hello`)*/

fetch('http://jsonplaceholder.typicode.com/todos/1')
.then(function (reponse) {return reponse.json()})
.then(function (json) {return console.log(json)});



// Getting all to do list item
fetch('http://jsonplaceholder.typicode.com/todos')
.then(function (reponse) {return reponse.json()})
.then(function (json) {

	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);

});

// Getting a specific todo list item.
fetch('http://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`The item "${json}" on the list has a status of ${json.completed}`))


// Create a todo list item using POST method
fetch('http://jsonplaceholder.typicode.com/todos', {
	method: "POST",
	headers: {
		'content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: false,
		userId: 1
	})
})
.then(function (reponse) {return reponse.json()})
.then(function (json) {return console.log(json)});

//Update a todo list item using PUT method
fetch('http://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'content-type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		dataCompleted: "Pending",
		userId: 1
	})
})
.then(function (reponse) {return reponse.json()})
.then(function (json) {return console.log(json)});


//Updating a todo list item using PATCH method
fetch('http://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'content-type': 'application/json'
	},
	body: JSON.stringify({
		status: "Completed",
		dataCompleted: "07/09/21"
	})
})
.then((reponse) => reponse.json())
.then((json) => console.log(json));


// Delete a todo list item
/*
	Delete: http://jsonplaceholder.typicode.com/todos/1

	test every method and screen shot the result from the postman and screenshot to the a1 folder
	push to gitlab, s33
	add link to boodle
	pass on or before 9:15

*/
fetch('http://jsonplaceholder.typicode.com/todos/1')
.then((reponse) => reponse.json())
.then((json) => console.log(json));