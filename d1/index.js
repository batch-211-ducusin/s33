//Itroduction to Postman and REST
/*
What is an API?

	Application Programming Interface

- The part of a server responsible for receiving request and sending responses.

What is it for?
- Hides the server beneath an interface layer
	* Hidden complexity makes apps easier to use
	* Sets the rules of interaction between front and back ends of an application, improving security.
	

What is REST?
- REpresentational State Transfer
	an architectural style for providing standards for communication between computer systems.

What problem does it solve?
- The need to separate user interface concerns of the client from data storage concerns of the server.

How does it solve the problem?
* Statelessness
	- Server does not need to know client state and vice versa.
	- Every client request has all the information it needs to be processed by the API.
	- Made possible via resources and HTTP methods.

* Standardized Communication
	- Enables a decoupled server to understand, process, and respond to client request WITHOUT knowing client state.
	- Implemented via resources represented as Uniform Resource Identifier (URI) endpoints and HTTP methods
		> Resources are plural by convention
			Ex:
				> /api/photos (NEVER /api/photo)
				> /api/statuses



* Anatomy of a Client Request
	- Operation to be performed dictated by HTTP methods
		REST API MTHODs
			GET
			PUT
			POST
			DELETE

	- A path to the resource to be operated on
		URI endpoint

	- A header containing additional request information

	- A request body containing data (optional)

http methods
	URI endpoint
	header
	body
*/

console.log(`Hello World`);

// JAVASCRIPT SYNCHRONOUS AND ASYNCHRONOUS
/*
SYNCHRONOUS
	- codes runs in sequence. This means that each operation must wait for the previous one to complete before executing.

ASYNCHRONOUS
	- codes runs in parallel. This means that an operation can occur while another one is still being processed.

	- code execution is often preferreable in situations where execution can be blocked. Some examples of this are network requests, long-running calculations, file system operations, etc. Using ASYNCHRONOUS code in the browser ensures the page remains reponse and the user experience is mostly unaffected.
*/
//EXAMPLE:
console.log(`Hello World`);
// cosnole.log(`Hello again`); //Hindi na to gagana kase Synchronous si JS
/*for(let i = 0; i <= 1000; i++){
	console.log(i)
};*/
/*console.log(`Hello it's me`);*/

/*https://official-joke-api.appspot.com/jokes/programming/random*/ //this is an example of api

/*
	- API stands for Application Programming Interface.
	- An application programming inferface is a particular set of codes that allows software programs to communicate with each other.
	- An API is the interface through which you access someone else's code or through which someone else's code accesses yours.

	Example:
		Google APIs
			https://developers.google.com/identity/sign-in/web/sign-in
		Youtube API
			https://developers.google.com/youtube/iframe_api_reference

JSON Placeholder
https://jsonplaceholder.typicode.com/posts
*/

//FETCH API
/*console.log(fetch("https://jsonplaceholder.typicode.com/posts"))*/
/*
	- A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value/

	- A promise is in one of these three states:
		pending:
			* Initial state, neither fulfilled nor rejected.
		Fulfulled:
			* Operation was successfully completed.
		Rejected:
			* Operation failed.
*/


/*
	- By using the .then method, we can now check for the status of the promise
	- The "fetch" method will return a "promise" that resolves to be a "response" object
	- The ".then" method captures the "response" object and returns another "promise" which will eventually be "resolved" or "rejected"
	Syntax:
		fetch("URL").then((response) => {});
*/
/*fetch("https://jsonplaceholder.typicode.com/posts").then(response => console.log(response.status))

fetch("https://jsonplaceholder.typicode.com/posts")
.then(function (response) {console.log(response.status)})

fetch("https://jsonplaceholder.typicode.com/posts")
//use the "json" method from the "reponse" object to convert the data retrieved into JSON format to be used in our application.
.then((response) => response.json())
//Print the converted JSON value from the "fetch" request
//Using multiple ".then" method to create a promise chain.
.then((json) => console.log(json))*/




/*
	- The "async" and "await" keywords is another approach that can be used to achieve asynchronous codes.
	- Used in functions to indicate which portions of code should be waited.
	- Creates an asynchronous function.
*/

/*async function fetchData(){
	//Waits for the "fetch" method to complete then stores the value in the "result" variable.
	let result = await fetch("https://jsonplaceholder.typicode.com/posts")
	//Result returned by fetch is returned as a promise
	console.log(result);
	// The returned "response" is an object
	console.log(typeof result);
	// We cannot access the content of the "response" by directly accessing it's body properly.
	console.log(result.body);
	let json = await result.json();
	console.log(json);
};
fetchData();*/


//Getting a specific post
/*
- Retrieves a specific post following the Rest API (retrive, /posts/id:, GET)
*/

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(function (reponse) {return reponse.json()})
.then(function (json) {return console.log(json)});


 /*
	POSTMAN

	url: https://jsonplaceholder.typicode.com/posts/1
	method: GET
 */

 /*
	CREATING A POST

	syntax:
	fetch("url", options)
	.then((reponse) => {})
	.then((reponse) => {})
 */
//Create a new post following the REST API (create, /post/:id, POST)
fetch('https://jsonplaceholder.typicode.com/posts', {

	method: 'POST',
	headers: {
		'content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello World',
		userId: 1
	})

})
.then(function (reponse) {return reponse.json()})
.then(function (json) {return console.log(json)});

/*
postman

url: https://jsonplaceholder.typicode.com/posts
method: POST
body: raw + JSON
	{
		"title": "My First Blog Post",
		"body": "Hello World!",
		"userId": 1
	}
*/

//UPDATING A POST
// Updating a specific post following the REST API (update, /posts/:id, PUT)

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: "PUT",
	headers: {
		'content-type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Update post',
		body: 'Hello again!',
		userId: 1
	})
})
.then(function (reponse) {return reponse.json()})
.then(function (json) {return console.log(json)});

/*
	POSTMAN:
	url: https://jsonplaceholder.typicode.com/post/1
	method: PUT
	body: raw + json
		{
			"title": "My First Revised Blog Post",
			"body": "Hello there! I revised this a bit.",
			"userId": 1
		}
*/


//Updating a post using the PATCH method
/*
	- updates a specific post following the rest api (update, /post/:id,patch)
	- the differences between PUT and PATCH is the number of properties being changed
	- PATCh updates the parts
	- PUT updates the whole documents
*/

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: "PATCH",
	headers: {
		'content-type': 'application/json'
	},
	body: JSON.stringify({
		title: "Corrected post"
	})
})
.then(function (reponse) {return reponse.json()})
.then(function (json) {return console.log(json)});

/*
	POSTMAN
		url: https://jsonplaceholder.typicode.com/posts/1
		mathod: PATCH
		body raw + json
		{
			"title": "This is my final title"
		}
*/


//DELETING A POST
// DELETING a specific post following the rest api (delete, /post/:id, DELETE)

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: "DELETE",
	headers: {
		'content-type': 'application/json'
	},
})
.then(function (reponse) {return reponse.json()})
.then(function (json) {return console.log(json)});

/*
	POSTMAN
	url: https://jsonplaceholder.typicode.com/posts/1
	method: DELETE
*/


//FILTERING THE POST
/*
	- The data can be filtered by sending the userId along with the url
	- Information sent via the URL can be done by adding the question mark symbol (?)
	Syntax:
		individual Parameters:
		'url?parameterName=value'\
		multiple Parameters:
		'url?ParamA=valueA&paramB=valueB'
*/

fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(function (reponse) {return reponse.json()})
.then(function (json) {return console.log(json)});

fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3')
.then(function (reponse) {return reponse.json()})
.then(function (json) {return console.log(json)});


// RETRIEVE COMMENTS OF A SPECIFIC POST
// Retrieving comments for a specific post following the Rest API (retrieve, /post/id:, GET)

fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then(function (reponse) {return reponse.json()})
.then(function (json) {return console.log(json)});